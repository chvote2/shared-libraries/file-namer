<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ file-namer
  ~ %%
  ~ Copyright (C) 2016 - 2019 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ch.ge.ve.interfaces</groupId>
    <artifactId>file-namer</artifactId>
    <version>0.1.2</version>
    <packaging>jar</packaging>
    <description>File naming service for CHVote files exchange</description>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>CHVote Snapshots</name>
            <url>${env.MVN_DIST_SNAPSHOTS_URL}</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>CHVote Releases</name>
            <url>${env.MVN_DIST_RELEASES_URL}</url>
        </repository>
    </distributionManagement>

    <properties>
        <!-- Build parameters -->
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <!-- SonarQube parameters -->
        <sonar.jacoco.reportPaths>target/jacoco.exec</sonar.jacoco.reportPaths>
        <sonar.projectName>FileNamer</sonar.projectName>
        <!-- Dependencies versions -->
        <groovy.version>2.4.11</groovy.version>
        <spock.version>1.1-groovy-2.4</spock.version>
        <equalsverifier.version>3.0.3</equalsverifier.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
            <version>${groovy.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.spockframework</groupId>
            <artifactId>spock-core</artifactId>
            <version>${spock.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>nl.jqno.equalsverifier</groupId>
            <artifactId>equalsverifier</artifactId>
            <version>${equalsverifier.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.0.0-M1</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <version>1.6</version>
                <configuration>
                    <targetBytecode>${java.version}</targetBytecode>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>addTestSources</goal>
                            <goal>compileTests</goal>
                            <goal>removeTestStubs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.0</version>
                <configuration>
                    <append>true</append>
                    <destFile>${sonar.jacoco.reportPaths}</destFile>
                </configuration>
                <executions>
                    <execution>
                        <id>agent-for-unit-tests</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-site</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive

import java.util.regex.Pattern
import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

class ArchiveContentValidatorTest extends Specification {

  def asRegexGroup(String name, String regex) {
    return new ContentGroup(name, Pattern.compile(regex).asPredicate())
  }

  def "should return all entries by group when everything's OK"() {
    given: 'a validator configured with some expected names and some groups'
    def validator = new ArchiveContentValidator()
    validator.expectedEntries = ["first.xml", "second.json"]
    def echGroup = asRegexGroup("ech", "^eCH-.*\\.xml\$")
    validator.addExpectedGroup(echGroup)
    def epfGroup = asRegexGroup("epf", "^.*\\.epf\$")
    validator.addExpectedGroup(epfGroup)

    and: 'entries that match the expected scheme'
    def entries = [
            "first.xml", "second.json",
            "eCH-159-test-operation.xml",
            "printer_CC0_0-24.epf"
    ]

    when:
    def result = validator.validate(entries)

    then: 'the validation should work without throwing an exception'
    noExceptionThrown()
    and: 'the grouped entries should be available via the result object'
    result[echGroup] == [ "eCH-159-test-operation.xml" ]
    result[epfGroup] == [ "printer_CC0_0-24.epf" ]
  }

  def "content not matching any group's predicate should be placed in the DEFAULT group"() {
    given:
    def validator = new ArchiveContentValidator()
    def echGroup = asRegexGroup("ech", "^eCH-.*\\.xml\$")
    validator.addExpectedGroup(echGroup)

    and:
    def entries = [
            "first.xml",
            "eCH-159-test-operation.xml",
            "other-non-matching.xml"
    ]

    when:
    def result = validator.validate(entries)

    then:
    result[echGroup] == [ "eCH-159-test-operation.xml" ]
    result[ArchiveContentValidator.DEFAULT_GROUP] == [ "first.xml", "other-non-matching.xml" ]
  }

  def "AddExpectedGroup should fail if the group already exists"() {
    given:
    def groupName = "ech"
    def validator = new ArchiveContentValidator()
    validator.addExpectedGroup(asRegexGroup(groupName, "^eCH-.*\\.xml\$"))

    when:
    validator.addExpectedGroup(asRegexGroup(groupName, "^.*\\.xml\$"))

    then:
    thrown IllegalArgumentException
  }

  def "Validate should fail if an expected entry name is not found"() {
    given:
    def validator = new ArchiveContentValidator()
    validator.expectedEntries = ["first.xml", "second.json"]

    def entries = [ "first.xml" ]

    when:
    validator.validate(entries)

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'Some mandatory files are missing : [second.json]'
  }

  def "Validate should fail if no entry is found for one (or more) specified group"() {
    given:
    def validator = new ArchiveContentValidator()
    validator.addExpectedGroup(asRegexGroup("xml", "^.*\\.xml\$"))
    validator.addExpectedGroup(asRegexGroup("epf", "^.*\\.epf\$"))
    validator.addExpectedGroup(asRegexGroup("pdf", "^.*\\.pdf\$"))

    def entries = [ "one.xml", "two.xml" ]

    when:
    validator.validate(entries)

    then:
    def ex = thrown InvalidArchiveException
    ex.message =~ '^No files were found for some expected groups : \\[(epf|pdf), (epf|pdf)\\]$'
  }

  def "ContentGroup should override equals() and hashcode()"() {
    expect:
    EqualsVerifier.forClass(ContentGroup).verify()
  }
}

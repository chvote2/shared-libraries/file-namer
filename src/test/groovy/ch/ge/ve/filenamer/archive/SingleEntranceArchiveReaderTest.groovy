/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive

import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class SingleEntranceArchiveReaderTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  // reader must be closed after test >> cleanup()
  SingleEntranceArchiveReader reader

  void setup() {
    def archivePath = temporaryFolder.newFile("anArchive.zip").toPath()
    ZipHelper.zip(archivePath, [
            "first.xml": "first text",
            "second.xml": "second text",
            "third.xml": "third text"
    ], "UTF-8")

    reader = new SingleEntranceArchiveReader(archivePath)
  }

  void cleanup() {
    reader?.close()
  }

  def "should return the archive name"() {
    expect:
    reader.getArchiveName() == 'anArchive.zip'
  }

  def "should access to the entry names"() {
    expect:
    reader.entryNames.toArray() == ["first.xml", "second.xml", "third.xml"] as Object[]
  }

  def "entries should be read by name"() {
    expect:
    reader.getEntryAsInputStream("first.xml").text == "first text"
    reader.getEntryAsInputStream("second.xml").text == "second text"
    reader.getEntryAsInputStream("third.xml").text == "third text"
  }

  def "close() should be idempotent"() {
    when:
    3.times { reader.close() }

    then:
    noExceptionThrown()
  }

  def "Once closed, the reader cannot be used to access anything anymore"() {
    given:
    reader.close()

    when:
    reader.getEntryAsInputStream("first.xml")

    then:
    thrown IllegalStateException
  }

  def "should fail if called with a non-existing entry name"() {
    when:
    reader.getEntryAsInputStream("invalid_entry.bson")

    then:
    thrown RuntimeException
    // It is actually a NullPointerException (ZipFile impl.) - but as it is not very readable i prefer not check it
  }
}

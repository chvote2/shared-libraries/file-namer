/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive


import java.time.LocalDateTime
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import spock.util.mop.Use

@Use(ZipHelper)
class PrinterArchiveReaderTest extends Specification {

  private static final String UTF_8 = "UTF-8"

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  private List<String> expectedFilenames = [
          "eCH-0045.xml",
          "public-parameters.json",
          "election-set.json",
          "operation-configuration.json",
          "election-configurations.json",
          "operation_ref.xml",
          "printer-file_CC0.epf"
  ]

  def "should read the metadata from archive name"() {
    given:
    def archive = temporaryFolder.newFile("printer-archive_test-operation_printer-1_2018-08-06-11h44m10s.paf").toPath()
    archive.zip(expectedFilenames)

    when:
    PrinterArchiveReader reader = PrinterArchiveReader.readFrom(archive)

    then:
    reader.printerName == "printer 1"
    reader.operationName == "test operation"
    reader.creationDate.get() == LocalDateTime.of(2018, 8, 6, 11, 44, 10)
  }

  def "should fail when given an invalid archive file"() {
    given:
    def archive = temporaryFolder.newFile("printer-archive_test-operation_printer-1_2018-08-06-11h44m10s.paf").toPath()
    archive.zip(["file_a.xml", "file_b.json"])

    when:
    PrinterArchiveReader.withArchive(archive, {})

    then:
    thrown InvalidArchiveException
  }

  def "should provide access to its content via appropriately named getter methods"() {
    given:
    def archive = temporaryFolder.newFile("printer-archive_test-operation_printer-1_2018-08-06-11h44m10s.paf").toPath()
    archive.zip([
            "eCH-0045.xml": "The voters reference",
            "public-parameters.json": '{"public": "parameters"}',
            "election-set.json": '{"candidates": 6}',
            "operation-configuration.json": '{"configuration": null}',
            "election-configurations.json": '{"election1": {}, "election2: {}}',
            "eCH-0159.xml": "The votation reference",
            "eCH-0157.xml": "The election reference",
            "printer-file_CC0.epf": 'This is my only EPF'
    ], UTF_8)

    when:
    PrinterArchiveReader reader = PrinterArchiveReader.readFrom(archive)

    then:
    reader.votersReferenceSource.getText(UTF_8) == "The voters reference"
    reader.publicParameters.getText(UTF_8) == '{"public": "parameters"}'
    reader.electionSetSource.getText(UTF_8) == '{"candidates": 6}'
    reader.operationConfigurationSource.getText(UTF_8) == '{"configuration": null}'
    reader.electionConfigurationsSource.getText(UTF_8) == '{"election1": {}, "election2: {}}'
    reader.operationReferenceSources.collectEntries {[it.key, it.value.getText(UTF_8)]} == [
            "eCH-0157.xml": "The election reference",
            "eCH-0159.xml": "The votation reference"
    ]
    reader.privateCredentialsSources.collectEntries {[it.key, it.value.getText(UTF_8)]} == [
            "printer-file_CC0.epf": "This is my only EPF"
    ]
  }

  def "an archive with no operation references should not be valid"() {
    given:
    def archive = temporaryFolder.newFile("printer-archive_201809VP_BOH_2018-09-23-14h01m00s.paf").toPath()
    archive.zip(expectedFilenames - ["operation_ref.xml"])

    when:
    PrinterArchiveReader.withArchive(archive, {})

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'No files were found for some expected groups : [operation reference]'
  }

  def "an archive with no private credentials should not be valid"() {
    given:
    def archive = temporaryFolder.newFile("printer-archive_201809VP_BOH_2018-09-23-14h01m00s.paf").toPath()
    archive.zip(expectedFilenames - ["printer-file_CC0.epf"])

    when:
    PrinterArchiveReader.withArchive(archive, {})

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'No files were found for some expected groups : [private credentials]'
  }

  def "operation reference entries should be accessible by their name"() {
    def archive = temporaryFolder.newFile("printer-archive_test-operation_test-printer.paf").toPath()
    archive.zip([
            "eCH-0045.xml": "The voters reference",
            "public-parameters.json": '{"public": "parameters"}',
            "election-set.json": '{"candidates": 6}',
            "operation-configuration.json": '{"configuration": null}',
            "election-configurations.json": '{"election1": {}, "election2: {}}',
            "eCH-0159.xml": "The votation reference",
            "eCH-0157.xml": "The election reference",
            "printer-file_CC0.epf": 'This is my only EPF'
    ], UTF_8)

    when:
    PrinterArchiveReader reader = PrinterArchiveReader.readFrom(archive)

    then: 'one can read each reference independently'
    reader.getOperationReferenceNames().toSorted() == ["eCH-0157.xml", "eCH-0159.xml"]
    reader.getOperationReferenceSource("eCH-0157.xml").text == "The election reference"
    reader.getOperationReferenceSource("eCH-0159.xml").text == "The votation reference"

    when: 'querying for a non-operation-reference entry'
    reader.getOperationReferenceSource("election-set.json")

    then:
    thrown IllegalArgumentException
  }

  def "private credentials entries should be accessible by their name"() {
    def archive = temporaryFolder.newFile("printer-archive_test-operation_test-printer.paf").toPath()
    archive.zip([
            "eCH-0045.xml": "The voters reference",
            "public-parameters.json": '{"public": "parameters"}',
            "election-set.json": '{"candidates": 6}',
            "operation-configuration.json": '{"configuration": null}',
            "election-configurations.json": '{"election1": {}, "election2: {}}',
            "eCH-0159.xml": "The votation reference",
            "printer-file_CC0.epf": 'This is my first EPF',
            "printer-file_CC1.epf": 'This is my second EPF'
    ], UTF_8)

    when:
    PrinterArchiveReader reader = PrinterArchiveReader.readFrom(archive)

    then: 'one can read each reference independently'
    reader.getPrivateCredentialsNames().toSorted() == ["printer-file_CC0.epf", "printer-file_CC1.epf"]
    reader.getPrivateCredentialsSource("printer-file_CC0.epf").text == "This is my first EPF"
    reader.getPrivateCredentialsSource("printer-file_CC1.epf").text == "This is my second EPF"

    when: 'querying for a non-private-credentials entry'
    reader.getPrivateCredentialsSource("eCH-0159.xml")

    then:
    thrown IllegalArgumentException
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive

import java.nio.file.Files
import java.nio.file.StandardOpenOption
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ReentrantArchiveReaderTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  def anArchive() {
    def archivePath = temporaryFolder.newFile("anArchive.zip").toPath()
    ZipHelper.zip(archivePath, [
            "first.json": "first text",
            "second.json": "second text",
            "third.json": "third text"
    ], "UTF-8")
    return archivePath
  }

  def "should return the archive name"() {
    given:
    def name = "an-archive-created-for-tests.zip"
    def archive = temporaryFolder.newFile(name).toPath()

    expect:
    new ReentrantArchiveReader(archive).getArchiveName() == name
  }

  def "should access to the entry names"() {
    given:
    def archive = anArchive()
    def reader = new ReentrantArchiveReader(archive)

    expect:
    reader.entryNames.toArray() == ["first.json", "second.json", "third.json"] as Object[]
  }

  def "entries should be read by name"() {
    given:
    def reader = new ReentrantArchiveReader(anArchive())

    expect:
    reader.getEntryAsInputStream("first.json").text == "first text"
    reader.getEntryAsInputStream("second.json").text == "second text"
    reader.getEntryAsInputStream("third.json").text == "third text"
  }

  def "all entry streams should be independent"() {
    given: 'a reader accessing an archive'
    def reader = new ReentrantArchiveReader(anArchive())

    and: 'entries accessed as InputStreams'
    def res1 = reader.getEntryAsInputStream("first.json")
    def res2 = reader.getEntryAsInputStream("second.json")
    def res3 = reader.getEntryAsInputStream("third.json")

    when: 'the first stream is closed'
    res1.close()

    then: 'other resources should remain accessible'
    res2.text == "second text"
    res3.text == "third text"

    cleanup:
    [res1, res2, res3]*.close()
  }

  def "should fail if called with a non-existing entry name"() {
    given:
    def reader = new ReentrantArchiveReader(anArchive())

    when:
    reader.getEntryAsInputStream("invalid_entry.bson")

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'Entry "invalid_entry.bson" not found in anArchive.zip'
  }

  def "should close the stream holding the file if an IOException occurs before the InputStream has been returned"() {
    given:
    def encryptedArchive = temporaryFolder.newFolder("archive").toPath().resolve("encrypted.zip")
    Files.copy(Objects.requireNonNull(getClass().getResourceAsStream("/encrypted.zip"), "InputStream not found"), encryptedArchive)

    def reader = new ReentrantArchiveReader(encryptedArchive)

    when:
    reader.getEntryAsInputStream("test_me.txt")

    then: 'the original exception should have been wrapped into an UncheckedIOException'
    thrown UncheckedIOException

    and: 'the file is not locked, ie i can write to it'
    Files.write(encryptedArchive, "This is now a regular text file".bytes, StandardOpenOption.TRUNCATE_EXISTING)
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive


import java.time.LocalDateTime

class TallyArchiveMakerTest extends ArchiveMakerSpec {

  // Prepare a TallyArchiveMaker, setting all files using "createTestFile(..)"
  def prepareTallyArchiveMaker(String operationName) {
    return new TallyArchiveMaker(operationName)
            .withBallotsSource(createTestFile("my-test-ballots.json"))
            .withConfirmationsSource(createTestFile("my-test-confirmations.json"))
            .withCountingCirclesSource(createTestFile("my-test-counting-circles.json"))
            .withElectionSetForVerificationSource(createTestFile("my-test-election-set-for-verification.json"))
            .withGeneratorsSource(createTestFile("my-test-generators.json"))
            .withPartialDecryptionProofsSource(createTestFile("my-test-partial-decryption-proofs.json"))
            .withPartialDecryptionsSource(createTestFile("my-test-partial-decryptions.json"))
            .withPrimesSource(createTestFile("my-test-primes.json"))
            .withPublicCredentialsSource(createTestFile("my-test-public-credentials.json"))
            .withPublicKeyPartsSource(createTestFile("my-test-public-key-parts.json"))
            .withPublicParametersSource(createTestFile("my-test-public-parameters.json"))
            .withShuffleProofsSource(createTestFile("my-test-shuffle-proofs.json"))
            .withShufflesSource(createTestFile("my-test-shuffles.json"))
            .withOperationReferenceSources([ createTestFile("xx_operation.xml") ])
  }

  def "should create a complete archive with all the specified inputs"() {
    given: 'archive metadata'
    def operation = "Unit test operation"
    def creationDate = LocalDateTime.of(2018, 8, 3, 17, 59)

    and: 'a Maker object prepared with simple files'
    def tallyArchiveMaker = prepareTallyArchiveMaker(operation)
            .withCreationDate(creationDate)

    when:
    def archive = tallyArchiveMaker.make(outputDir)

    then: 'the archive should have been created with the appropriate name'
    archive.fileName as String == 'tally-archive_Unit-test-operation_2018-08-03-17h59m00s.taf'

    and: 'the archive should be a ZIP that contains all expected files with original data'
    ZipHelper.readEntries(archive, UTF_8) == [
            "ballots.json"                      : "my-test-ballots.json",
            "confirmations.json"                : "my-test-confirmations.json",
            "counting-circles.json"             : "my-test-counting-circles.json",
            "election-set-for-verification.json": "my-test-election-set-for-verification.json",
            "generators.json"                   : "my-test-generators.json",
            "partial-decryption-proofs.json"    : "my-test-partial-decryption-proofs.json",
            "partial-decryptions.json"          : "my-test-partial-decryptions.json",
            "primes.json"                       : "my-test-primes.json",
            "public-credentials.json"           : "my-test-public-credentials.json",
            "public-key-parts.json"             : "my-test-public-key-parts.json",
            "public-parameters.json"            : "my-test-public-parameters.json",
            "shuffle-proofs.json"               : "my-test-shuffle-proofs.json",
            "shuffles.json"                     : "my-test-shuffles.json",
            "xx_operation.xml"                  : "xx_operation.xml"
    ]
  }

  // Copied from PrinterArchiveMakerTest
  def "OperationReference sources should be appendable one by one"() {
    given:
    def tallyArchiveMaker = new TallyArchiveMaker("operation")

    when:
    tallyArchiveMaker.addOperationReferenceSource(createTestFile("op1.xml"))
    tallyArchiveMaker.addOperationReferenceSource(createTestFile("op2.xml"))

    then:
    tallyArchiveMaker.@operationReferenceSources.collect { it.getFileName().toString() } == [
            "op1.xml", "op2.xml"
    ]
  }

  // Copied from PrinterArchiveMakerTest
  def "OperationReference Sources require .xml extension files"() {
    given:
    def jsonFile = inputsDir.resolve("operation-reference.json")

    when:
    new TallyArchiveMaker("operation").withOperationReferenceSources([jsonFile])

    then:
    def ex = thrown IllegalArgumentException
    ex.message == '<operation reference> sources are required to have a ".xml" extension, but found : "operation-reference.json"'
  }

  def "should fail if a resource is missing"() {
    given: 'a Maker prepared with missing primes'
    def tallyArchiveMaker = new TallyArchiveMaker("operation")
            .withCreationDate(LocalDateTime.now())
            .withBallotsSource(createTestFile("my-test-ballots.json"))
            .withConfirmationsSource(createTestFile("my-test-confirmations.json"))
            .withCountingCirclesSource(createTestFile("my-test-counting-circles.json"))
            .withElectionSetForVerificationSource(createTestFile("my-test-election-set-for-verification.json"))
            .withGeneratorsSource(createTestFile("my-test-generators.json"))
            .withPartialDecryptionProofsSource(createTestFile("my-test-partial-decryption-proofs.json"))
            .withPartialDecryptionsSource(createTestFile("my-test-partial-decryptions.json"))
            .withPublicCredentialsSource(createTestFile("my-test-public-credentials.json"))
            .withPublicKeyPartsSource(createTestFile("my-test-public-key-parts.json"))
            .withPublicParametersSource(createTestFile("my-test-public-parameters.json"))
            .withShuffleProofsSource(createTestFile("my-test-shuffle-proofs.json"))
            .withShufflesSource(createTestFile("my-test-shuffles.json"))
            .withOperationReferenceSources([ createTestFile("xx_operation.xml") ])

    when:
    tallyArchiveMaker.make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'primes.json has not been specified.'
  }

  def "should fail if operation references are missing"() {
    given: 'a Maker object prepared with simple files but no operation reference'
    def tallyArchiveMaker = new TallyArchiveMaker("operation")
            .withCreationDate(LocalDateTime.now())
            .withBallotsSource(createTestFile("my-test-ballots.json"))
            .withConfirmationsSource(createTestFile("my-test-confirmations.json"))
            .withCountingCirclesSource(createTestFile("my-test-counting-circles.json"))
            .withElectionSetForVerificationSource(createTestFile("my-test-election-set-for-verification.json"))
            .withGeneratorsSource(createTestFile("my-test-generators.json"))
            .withPartialDecryptionProofsSource(createTestFile("my-test-partial-decryption-proofs.json"))
            .withPartialDecryptionsSource(createTestFile("my-test-partial-decryptions.json"))
            .withPrimesSource(createTestFile("my-test-primes.json"))
            .withPublicCredentialsSource(createTestFile("my-test-public-credentials.json"))
            .withPublicKeyPartsSource(createTestFile("my-test-public-key-parts.json"))
            .withPublicParametersSource(createTestFile("my-test-public-parameters.json"))
            .withShuffleProofsSource(createTestFile("my-test-shuffle-proofs.json"))
            .withShufflesSource(createTestFile("my-test-shuffles.json"))

    when:
    tallyArchiveMaker.make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'Operation references (eCH-157 / eCH-159) have not been specified.'
  }

  def "should fail if the generated archive name does not match the pattern"() {
    given: 'a non-conform operation name'
    def operationName = "#invalid#"

    when:
    prepareTallyArchiveMaker(operationName).make(temporaryFolder.newFolder().toPath())

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'The name "tally-archive_#invalid#.taf" does not match the expected pattern for a TallyArchive'
  }

  def "should fail if output is not a directory"() {
    when:
    prepareTallyArchiveMaker("operation").make(temporaryFolder.newFile().toPath())

    then:
    thrown IllegalArgumentException
  }
}

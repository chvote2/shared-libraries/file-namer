/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer


import java.time.LocalDateTime
import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

class PrinterArchiveFileNameTest extends Specification {

  def "should format the filename with all information"() {
    given:
    def creationDate = LocalDateTime.of(2018, 6, 3, 15, 14)
    def archiveFilename = new PrinterArchiveFileName("my printer", "an operation", creationDate)

    expect:
    archiveFilename.toString() == "printer-archive_an-operation_my-printer_2018-06-03-15h14m00s.paf"
  }

  def "should format the filename without a creationDate"() {
    given:
    def archiveFilename = new PrinterArchiveFileName("my printer", "an operation")

    expect:
    archiveFilename.toString() == "printer-archive_an-operation_my-printer.paf"
  }

  def "should fail to create an instance with forbidden inputs"() {
    given:
    def wrongPrinterName = "#invalid#"

    when:
    new PrinterArchiveFileName(wrongPrinterName, "operation")

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'The name "printer-archive_operation_#invalid#.paf" does not match the expected pattern for a PrinterArchive'
  }

  def "should be able to parse a filename with all possible information"() {
    given:
    def filename = "printer-archive_test-operation_test-printer_2018-08-20-09h12m14s.paf"

    when:
    def holder = PrinterArchiveFileName.parse(filename)

    then:
    holder.operationName == "test operation"
    holder.printerName == "test printer"
    holder.creationDate == Optional.of(LocalDateTime.of(2018, 8, 20, 9, 12, 14))
  }

  def "should be able to parse a filename without a creationDate"() {
    given:
    def filename = "printer-archive_test-operation_test-printer.paf"

    when:
    def holder = PrinterArchiveFileName.parse(filename)

    then:
    holder.operationName == "test operation"
    holder.printerName == "test printer"
    holder.creationDate.isPresent() == Boolean.FALSE
  }

  def "should refuse to parse an incorrect filename"() {
    given:
    def wrongFileName = "invalid-archive_an-operation_my-printer_2018-06-03-15h14m00s.paf"

    when:
    PrinterArchiveFileName.parse(wrongFileName)

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'The name "invalid-archive_an-operation_my-printer_2018-06-03-15h14m00s.paf" does not match the expected pattern for a PrinterArchive'
  }

  def "it must override equals() and hashCode()"() {
    expect:
    EqualsVerifier.forClass(PrinterArchiveFileName).withOnlyTheseFields("fileName").verify()
  }

  def "should be able to parse a formatted filename and vice versa"(String printerName, String operationName, LocalDateTime creationDate) {
    when:
    def formatted = new PrinterArchiveFileName(printerName, operationName, creationDate).toString()
    def parsed = PrinterArchiveFileName.parse(formatted)

    then:
    formatted == parsed.toString()
    parsed.printerName == printerName
    parsed.operationName == operationName
    parsed.creationDate == Optional.ofNullable(creationDate)

    where:
    printerName | operationName             | creationDate
    "boh-vprt"  | "spock test operation"    | LocalDateTime.of(2018, 8, 19, 8, 0)
    "boh-vprt"  | "spock test operation II" | null
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Main entry point to generate a file name
 */
public final class FileNamer {

  private static final String ECH_0110_PREFIX                   = "eCH-0110";
  private static final String ECH_0222_PREFIX                   = "eCH-0222";
  private static final String ECH_0228_PREFIX                   = "eCH-0228";
  private static final String ECH_228_ARCHIVE_PREFIX            = "eCH-0228-archive";
  private static final String CONSOLIDATED_VOTERS_REPORT_PREFIX = "consolidated-voters-report";
  private static final String VOTERS_REPORT_PREFIX              = "voters-report";

  private static final String ZIP_EXTENSION  = "zip";
  private static final String XML_EXTENSION  = "xml";
  private static final String PDF_EXTENSION  = "pdf";

  /**
   * Formatter based on the standard pattern for dates in file names : "yyyy-MM-dd-HH'h'mm'm'ss's'".
   */
  public static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd-HH'h'mm'm'ss's'");

  private FileNamer() {
    throw new AssertionError("this class is not supposed to be instantiated");
  }

  /**
   * Generates the file name for an eCH-228 archive (ie the voting cards archive intended to the printers).
   *
   * @param operationName the operation's short name
   * @param printerName   the name of the printer
   * @param creationDate  the creation date of the file
   *
   * @return a well formatted name for the corresponding archive
   */
  public static String ech228Archive(String operationName, String printerName, LocalDateTime creationDate) {
    return buildFileName(ECH_228_ARCHIVE_PREFIX, operationName, printerName, null, creationDate, ZIP_EXTENSION);
  }

  /**
   * Generates the file name for a printer file generated from a lot definition.
   * <p>
   *   A "lot" refers to a <em>group</em> of voting cards, assembled under a label describing the
   *   business use case they were created for.
   * </p>
   *
   * @param printingLotType the group of voting cards forming the lot
   * @param operationName   the operation's short name
   * @param lotName         the name given to the lot
   * @param printerName     the name of the associated printer
   * @param creationDate    the creation date of the file
   *
   * @return a well formatted name for the corresponding printer file
   */
  public static String lotPrinterFile(PrintingLotType printingLotType, String operationName, String lotName,
                                      String printerName, LocalDateTime creationDate) {
    return buildFileName(printingLotType.getLabel(), operationName, printerName, CaseFormat.toKebabCase(lotName),
                         creationDate, null);
  }

  /**
   * Generates the file name for an eCH-0110 result file.
   *
   * @param operationName the operation's short name
   * @param creationDate  the creation date of the file
   *
   * @return a well formatted name for the corresponding file
   */
  public static String ech110File(String operationName, LocalDateTime creationDate) {
    return buildFileName(ECH_0110_PREFIX, operationName, null, null, creationDate, XML_EXTENSION);
  }

  /**
   * Generates the file name for an eCH-0222 result file.
   *
   * @param operationName the operation's short name
   * @param creationDate  the creation date of the file
   *
   * @return a well formatted name for the corresponding file
   */
  public static String ech222File(String operationName, LocalDateTime creationDate) {
    return buildFileName(ECH_0222_PREFIX, operationName, null, null, creationDate, XML_EXTENSION);
  }

  /**
   * Generates the file name for an eCH-0228 printer file.
   *
   * @param operationName the operation's short name
   * @param printerName   the name of the printer
   * @param creationDate  the creation date of the file
   *
   * @return a well formatted name for the corresponding printer file
   */
  public static String ech228File(String operationName, String printerName, LocalDateTime creationDate) {
    return buildFileName(ECH_0228_PREFIX, operationName, printerName, null, creationDate, XML_EXTENSION);
  }

  /**
   * Generates the file name for the voters report.
   *
   * @param operationName the operation's short name
   * @param registerName  the register's name
   * @param importDate    the register's import date
   *
   * @return a well formatted name for the corresponding report
   */
  public static String votersReport(String operationName, String registerName, LocalDateTime importDate) {
    return buildFileName(VOTERS_REPORT_PREFIX, operationName, registerName, null, importDate, PDF_EXTENSION);
  }

  /**
   * Generates the file name for the consolidated voters report.
   *
   * @param operationName the operation's short name
   * @param operationDate the operation's date
   *
   * @return a well formatted name for the corresponding report
   */
  public static String consolidatedVotersReport(String operationName, LocalDateTime operationDate) {
    return buildFileName(CONSOLIDATED_VOTERS_REPORT_PREFIX, operationName, null, null, operationDate, PDF_EXTENSION);
  }

  private static String buildFileName(String prefix,
                                      String operationName,
                                      String info1,
                                      String info2,
                                      LocalDateTime date,
                                      String extension) {

    return String.format("%s_%s%s%s_%s%s",
                         prefix,
                         CaseFormat.toKebabCase(operationName),
                         info1 != null ? String.format("_%s", CaseFormat.toKebabCase(info1)) : "",
                         info2 != null ? String.format("_%s", info2) : "",
                         DATE_TIME_FORMATTER.format(date),
                         extension != null ? String.format(".%s", extension) : "");
  }
}

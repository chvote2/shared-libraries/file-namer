/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.io.InputStream;
import java.util.stream.Stream;

/**
 * Abstraction of the access to an archive.
 * <p>
 *   Implementation could optimize access for certain scenarios, vary the encryption mechanisms, etc.
 *   An archive should have a name and entries, whatever the implementation.
 * </p>
 */
interface ArchiveReader {

  /**
   * @return the name of the archive
   */
  String getArchiveName();

  /**
   * @return a stream of all entries names
   */
  Stream<String> getEntryNames();

  /**
   * Access an entry as an {@link InputStream}.
   * <p>
   *   This method opens a stream allowing to read the entry's content.
   *   It is the caller's responsibility to close the stream when finished.
   *   Closing an entry's {@code InputStream} may, or may not depending on the implementation, release
   *   the archive's resource at the lower level (eg filesystem).
   * </p>
   *
   * @param entryName name of the entry to access
   * @return an inputStream to read the entry's content
   */
  InputStream getEntryAsInputStream(String entryName);

}

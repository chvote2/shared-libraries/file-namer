/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.nio.file.Path;
import java.util.regex.Pattern;

/**
 * Package-private class that shares the constants between archive maker and reader.
 */
class Internals {

  private Internals() {
    throw new AssertionError("Not instantiable");
  }

  static final String ECH_0045                = "eCH-0045.xml";
  static final String PUBLIC_PARAMETERS       = "public-parameters.json";
  static final String ELECTION_SET            = "election-set.json";
  static final String OPERATION_CONFIGURATION = "operation-configuration.json";
  static final String ELECTION_CONFIGURATIONS = "election-configurations.json";


  static final String  BALLOTS                       = "ballots.json";
  static final String  CONFIRMATIONS                 = "confirmations.json";
  static final String  COUNTING_CIRCLES              = "counting-circles.json";
  static final String  ELECTION_SET_FOR_VERIFICATION = "election-set-for-verification.json";
  static final String  GENERATORS                    = "generators.json";
  static final String  PARTIAL_DECRYPTION_PROOFS     = "partial-decryption-proofs.json";
  static final String  PARTIAL_DECRYPTIONS           = "partial-decryptions.json";
  static final String  PRIMES                        = "primes.json";
  static final String  PUBLIC_CREDENTIALS            = "public-credentials.json";
  static final String  PUBLIC_KEY_PARTS              = "public-key-parts.json";
  static final String  SHUFFLE_PROOFS                = "shuffle-proofs.json";
  static final String  SHUFFLES                      = "shuffles.json";

  static final ContentGroup OPERATION_REFERENCE_GROUP = new ContentGroup("operation reference",
                                                                         Pattern.compile("^.*\\.xml$").asPredicate());
  static final ContentGroup PRIVATE_CREDENTIALS_GROUP = new ContentGroup("private credentials",
                                                                         Pattern.compile("^.*\\.epf$").asPredicate());

  static Path verifyOperationReference(Path value) {
    return verifyExtension(value, OPERATION_REFERENCE_GROUP, "xml");
  }

  static Path verifyPrivateCredentials(Path value) {
    return verifyExtension(value, PRIVATE_CREDENTIALS_GROUP, "epf");
  }

  private static Path verifyExtension(Path path, ContentGroup group, String extension) {
    final String fileName = path.getFileName().toString();
    if ( ! group.getCondition().test(fileName)) {
      throw new IllegalArgumentException(
          String.format("<%s> sources are required to have a \".%s\" extension, but found : \"%s\"",
                        group.getName(), extension, fileName));
    }
    return path;
  }

}

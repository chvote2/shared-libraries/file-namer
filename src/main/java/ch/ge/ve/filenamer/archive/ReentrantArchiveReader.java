/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * An {@link ArchiveReader} where the archive is opened every time an operation is requested.
 * <p>
 *   This implementation best fits when the instantiator does not control accesses to the archive's content,
 *   and want to ensure the user doesn't need to manage the filesystem locks to the archive file itself.
 * </p>
 * <p>
 *   Pros :
 *   <ul>
 *     <li>every entry (as InputStream), every operation is independent of any global archive stream state</li>
 *     <li>no need for the caller to manage the archive InputStream</li>
 *   </ul>
 *
 *   Cons :
 *   <ul>
 *     <li>more cumbersome implementation</li>
 *     <li>archive file is opened multiple times (perf impact ?)</li>
 *     <li>what if the archive is modified / moved between two calls ?</li>
 *   </ul>
 */
class ReentrantArchiveReader implements ArchiveReader {

  private final Path archive;
  private List<String> entries;

  public ReentrantArchiveReader(Path archive) {
    this.archive = archive;
  }

  @Override
  public String getArchiveName() {
    return archive.getFileName().toString();
  }

  @Override
  public Stream<String> getEntryNames() {
    // Entries must be collected first and cannot be streamed while they are read, because we need to close the FS lock
    if (entries == null) {
      this.readEntries();
    }
    return entries.stream();
  }

  private void readEntries() {
    try (ZipInputStream zip = new ZipInputStream(Files.newInputStream(archive))) {
      final List<String> names = new ArrayList<>();
      ZipEntry zipEntry = zip.getNextEntry();
      while (zipEntry != null) {
        names.add(zipEntry.getName());
        zip.closeEntry();
        zipEntry = zip.getNextEntry();
      }
      this.entries = names;
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Opens the archive and returns an {@link InputStream} of the requested entry.
   * <p>
   *   The InputStream of the archive itself is bound to the one of the entry : closing the returned
   *   resource will close all the chain down to the archive file itself.
   * </p>
   *
   * @param entryName name of the entry to access
   * @return an inputStream to read the entry's content
   */
  @SuppressWarnings("squid:S2095") // InputStreams are "embedded" in EntryInputStream >> closed by the caller
  @Override
  public InputStream getEntryAsInputStream(String entryName) {
    InputStream resourceInputStream = null;
    try {
      resourceInputStream = Files.newInputStream(archive);
      ZipInputStream zipInputStream = new ZipInputStream(resourceInputStream);

      ZipEntry nextEntry = zipInputStream.getNextEntry();
      while (nextEntry != null && !nextEntry.getName().equals(entryName)) {
        zipInputStream.closeEntry();
        nextEntry = zipInputStream.getNextEntry();
      }

      if (nextEntry == null) {
        zipInputStream.close();
        throw new IllegalArgumentException(String.format("Entry \"%s\" not found in %s",
                                                         entryName, archive.getFileName()));
      }

      return new EntryInputStream(zipInputStream);
    } catch (IOException e) {
      closeNullableStream(resourceInputStream);
      throw new UncheckedIOException(e);
    }
  }

  private static void closeNullableStream(InputStream inputStream) {
    if (inputStream != null) {
      try {
        inputStream.close();
      } catch (IOException e) {
        throw new UncheckedIOException("An exception occurred while closing the InputStream", e);
      }
    }
  }

  // Hides ZipInputStream behind this implementation so that the consumer cannot "easily" navigate between entries
  private static class EntryInputStream extends InputStream {

    private final InputStream delegate;

    private EntryInputStream(InputStream delegate) {
      this.delegate = delegate;
    }

    @Override
    public int read() throws IOException {
      return delegate.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
      return delegate.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
      return delegate.read(b, off, len);
    }

    @Override
    public long skip(long n) throws IOException {
      return delegate.skip(n);
    }

    @Override
    public int available() throws IOException {
      return delegate.available();
    }

    @Override
    public void close() throws IOException {
      delegate.close();
    }

    @Override
    public synchronized void mark(int readlimit) {
      delegate.mark(readlimit);
    }

    @Override
    public synchronized void reset() throws IOException {
      delegate.reset();
    }

    @Override
    public boolean markSupported() {
      return delegate.markSupported();
    }
  }
}

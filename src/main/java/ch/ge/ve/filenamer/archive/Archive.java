/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Internal class to delegate the action of creating an archive for the chvote project.
 */
final class Archive {

  private final String name;

  private final Map<String, Path> resources;

  Archive(String name) {
    this.name = name;
    this.resources = new LinkedHashMap<>();
  }

  /**
   * Adds the given Path's content to the Zip, specifying its entry name.
   *
   * @param source where to get the content to zip
   * @param entryName name of the entry in the zip
   */
  public void append(Path source, String entryName) {
    if (source == null) {
      throw new InvalidArchiveException(entryName + " has not been specified.");
    }

    resources.put(entryName, source);
  }

  /**
   * Adds the given Path's content to the Zip, using its filename as the entry name.
   *
   * @param source where to get the content to zip
   */
  public void append(Path source) {
    this.append(source, source.getFileName().toString());
  }

  /**
   * Adds a bunch of Paths' content to the Zip, using their filenames as the entry names.
   *
   * @param sources where to get the content to zip
   * @param name name of the collection, used in the error message
   */
  public void append(Collection<? extends Path> sources, String name) {
    if (sources.isEmpty()) {
      throw new InvalidArchiveException(name + " have not been specified.");
    }

    for (Path source : sources) {
      append(source);
    }
  }

  /**
   * Creates the archive in the given directory.
   * The archive will be filled with all appended resources, in the same order.
   *
   * @param outputDirectory where to write the archive artifact
   * @return a path to the created archive
   */
  public Path create(Path outputDirectory) {
    if ( ! Files.isDirectory(outputDirectory)) {
      throw new IllegalArgumentException("Given output directory is no directory : " + outputDirectory);
    }

    final Path archivePath = outputDirectory.resolve(name);
    try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(archivePath))) {

      for (Map.Entry<String, Path> entry : resources.entrySet()) {
        zipOutputStream.putNextEntry(new ZipEntry(entry.getKey()));
        Files.copy(entry.getValue(), zipOutputStream);
        zipOutputStream.closeEntry();
      }

    } catch (IOException e) {
      throw new UncheckedIOException("Failed to create the archive " + name, e);
    }

    return archivePath;
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * An {@link ArchiveReader}, where the archive InputStream is managed in a "Closeable" manner.
 * <p>
 *   This implementation fits when the instantiator can control the lifecycle of the opening of the archive.
 *   Please note than once the reader is closed, no more access can be made to the archive.
 * </p>
 * <p>
 *   Pros :
 *   <ul>
 *     <li>straight-forward implementation</li>
 *     <li>resource is opened only once, filesystem locking is under control</li>
 *     <li>holds the lock on the resource during lifecycle</li>
 *   </ul>
 *
 *   Cons :
 *   <ul>
 *     <li>caller must manage the state ("close()" the reader)</li>
 *   </ul>
 */
class SingleEntranceArchiveReader implements ArchiveReader, Closeable {

  private final ZipFile zipFile;

  public SingleEntranceArchiveReader(Path archive) throws IOException {
    this.zipFile = new ZipFile(archive.toFile());
  }

  @Override
  public String getArchiveName() {
    return Paths.get(zipFile.getName()).getFileName().toString();
  }

  @Override
  public Stream<String> getEntryNames() {
    return zipFile.stream().map(ZipEntry::getName);
  }

  /**
   * Opens an {@link InputStream} of the requested entry.
   * <p>
   *   This InputStream of is bound to the one of the archive itself : closing it (or closing the reader)
   *   will close this entry's stream. But the reverse is not true.
   * </p>
   *
   * @param entryName name of the entry to access
   * @return an inputStream to read the entry's content
   */
  @Override
  public InputStream getEntryAsInputStream(String entryName) {
    final ZipEntry zipEntry = zipFile.getEntry(entryName);
    try {
      return zipFile.getInputStream(zipEntry);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Closes this stream and releases the underlying archive resource.
   * After the reader has been closed, any attempt to read data from the archive will fail
   * with an {@code IllegalStateException}.
   * <p>
   *   This method can be called multiple times without any failure to be expected :
   *   if the resource is already closed the call actually is a no-op.
   * </p>
   *
   * @throws IOException if an I/O error occurs while closing the archive resource
   */
  @Override
  public void close() throws IOException {
    zipFile.close();
  }
}

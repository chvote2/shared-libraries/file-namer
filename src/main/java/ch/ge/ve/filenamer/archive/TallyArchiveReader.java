/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import ch.ge.ve.filenamer.TallyArchiveFileName;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Component that reads a "tally archive".
 * <p>
 *   When given the path to an archive artifact, a {@code TallyArchiveReader} can access its metadata,
 *   and easily provide streams to each data it contains.
 * </p>
 * <p>
 *   This reader keeps hidden the actual structure of the archive. Used in conjunction with {@link TallyArchiveMaker},
 *   this structure is left abstract and could be changed with minimal impact on the rest of the codebase.
 * </p>
 */
public class TallyArchiveReader {

  private final TallyArchiveFileName fileName;
  private final List<String>         operationReferenceFilenames;
  private final ArchiveReader        reader;

  private TallyArchiveReader(ArchiveReader archiveReader) {
    this.reader = archiveReader;
    this.fileName = TallyArchiveFileName.parse(reader.getArchiveName());

    final ArchiveContentValidator validator = new ArchiveContentValidator();
    validator.setExpectedEntries(allFixedFileNames());
    validator.addExpectedGroup(Internals.OPERATION_REFERENCE_GROUP);

    final Map<ContentGroup, List<String>> entries = validator.validate(reader.getEntryNames());
    this.operationReferenceFilenames = Collections.unmodifiableList(entries.get(Internals.OPERATION_REFERENCE_GROUP));
  }

  /**
   * Instantiate a reader, bound to a specific archive defined by its path.
   * <p>
   *   This instance holds a reference to the path to the archive, but every access to any of its resources
   *   will cause a new stream to be opened from it. This is adequate if you want to access parts of the archive
   *   on a (relative) long period. If you need to access all (or part) of the resources once and then forget about
   *   the archive, prefer the {@link #withArchive(Path, Consumer)} construct.
   * </p>
   * <p>
   *   Note that the archive will be verified first, so that any invalid archive will cause this constructor
   *   to fail, and the archive will not be readable.
   * </p>
   *
   * @param archivePath path to the archive to read
   *
   * @throws InvalidArchiveException if the path does not denote a regular tally archive
   * @throws IllegalArgumentException if the archive name does not match the expected format
   * @throws UncheckedIOException if an I/O error occurs
   */
  public static TallyArchiveReader readFrom(Path archivePath) {
    return new TallyArchiveReader(new ReentrantArchiveReader(archivePath));
  }

  /**
   * Gets a {@link TallyArchiveReader} to perform some operation, then closes all filesystem resources.
   * <p>
   *   The reader holds a single reference to the underlying file in the filesystem. It will be passed
   *   to the consumer that can freely access all of its resources. When done (or if an exception occurs),
   *   all filesystem locks are released and any subsequent call to read one of its resources will fail.
   * </p>
   * <p>
   *   Note that the archive will be verified first, so that any invalid archive will cause this method
   *   to fail before the consumer is called.
   * </p>
   *
   * @param archivePath path to the archive to read
   * @param consumer the action to perform using the reader
   *
   * @throws InvalidArchiveException if the path does not denote a regular tally archive
   * @throws IllegalArgumentException if the archive name does not match the expected format
   * @throws UncheckedIOException if an I/O error occurs
   */
  public static void withArchive(Path archivePath, Consumer<TallyArchiveReader> consumer) {
    try (SingleEntranceArchiveReader reader = new SingleEntranceArchiveReader(archivePath)) {
      consumer.accept(new TallyArchiveReader(reader));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private static List<String> allFixedFileNames() {
    return Arrays.asList(
        Internals.BALLOTS,
        Internals.CONFIRMATIONS,
        Internals.COUNTING_CIRCLES,
        Internals.ELECTION_SET_FOR_VERIFICATION,
        Internals.GENERATORS,
        Internals.PARTIAL_DECRYPTION_PROOFS,
        Internals.PARTIAL_DECRYPTIONS,
        Internals.PRIMES,
        Internals.PUBLIC_CREDENTIALS,
        Internals.PUBLIC_KEY_PARTS,
        Internals.PUBLIC_PARAMETERS,
        Internals.SHUFFLE_PROOFS,
        Internals.SHUFFLES
    );
  }

  /**
   * @return the name of the operation described in this archive
   */
  public String getOperationName() {
    return fileName.getOperationName();
  }

  /**
   * @return date when the archive has been created
   */
  public Optional<LocalDateTime> getCreationDate() {
    return fileName.getCreationDate();
  }

  /**
   * Access to the ballots data.
   *
   * @return data-stream of the "ballots" source in the protocol model
   */
  public InputStream getBallotsSource() {
    return reader.getEntryAsInputStream(Internals.BALLOTS);
  }

  /**
   * Access to the confirmations data.
   *
   * @return data-stream of the "confirmations" source in the protocol model
   */
  public InputStream getConfirmationsSource() {
    return reader.getEntryAsInputStream(Internals.CONFIRMATIONS);
  }

  /**
   * Access to the countingCircles data.
   *
   * @return data-stream of the "counting-circles" source : number of voters per counting circle and per DOI
   */
  public InputStream getCountingCirclesSource() {
    return reader.getEntryAsInputStream(Internals.COUNTING_CIRCLES);
  }

  /**
   * Access to the electionSetForVerification data.
   *
   * @return data-stream of the "election-set" source in the protocol verification model
   */
  public InputStream getElectionSetForVerificationSource() {
    return reader.getEntryAsInputStream(Internals.ELECTION_SET_FOR_VERIFICATION);
  }

  /**
   * Access to the generators data.
   *
   * @return data-stream of the "generators" source in the protocol model
   */
  public InputStream getGeneratorsSource() {
    return reader.getEntryAsInputStream(Internals.GENERATORS);
  }

  /**
   * Access to the partialDecryptionProofs data.
   *
   * @return data-stream of the "partialDecryptionProofs" source in the protocol model
   */
  public InputStream getPartialDecryptionProofsSource() {
    return reader.getEntryAsInputStream(Internals.PARTIAL_DECRYPTION_PROOFS);
  }

  /**
   * Access to the partialDecryptions data.
   *
   * @return data-stream of the "partialDecryptions" source in the protocol model
   */
  public InputStream getPartialDecryptionsSource() {
    return reader.getEntryAsInputStream(Internals.PARTIAL_DECRYPTIONS);
  }

  /**
   * Access to the primes data.
   *
   * @return data-stream of the "primes" source in the protocol model
   */
  public InputStream getPrimesSource() {
    return reader.getEntryAsInputStream(Internals.PRIMES);
  }

  /**
   * Access to the publicCredentials data.
   *
   * @return data-stream of the "publicCredentials" source in the protocol model
   */
  public InputStream getPublicCredentialsSource() {
    return reader.getEntryAsInputStream(Internals.PUBLIC_CREDENTIALS);
  }

  /**
   * Access to the publicKeyParts data.
   *
   * @return data-stream of the "publicKeyParts" source in the protocol model
   */
  public InputStream getPublicKeyPartsSource() {
    return reader.getEntryAsInputStream(Internals.PUBLIC_KEY_PARTS);
  }

  /**
   * Access to the publicParameters data.
   *
   * @return data-stream of the "public parameters" source in the protocol model
   */
  public InputStream getPublicParameters() {
    return reader.getEntryAsInputStream(Internals.PUBLIC_PARAMETERS);
  }

  /**
   * Access to the shuffleProofs data.
   *
   * @return data-stream of the "shuffleProofs" source in the protocol model
   */
  public InputStream getShuffleProofsSource() {
    return reader.getEntryAsInputStream(Internals.SHUFFLE_PROOFS);
  }

  /**
   * Access to the shuffles data.
   *
   * @return data-stream of the "shuffles" source in the protocol model
   */
  public InputStream getShufflesSource() {
    return reader.getEntryAsInputStream(Internals.SHUFFLES);
  }

  /**
   * @return a list of the names of all operation references entries
   */
  public List<String> getOperationReferenceNames() {
    return operationReferenceFilenames;
  }

  /**
   * Access to one operationReference entry. Also known as "eCH-0157" / "eCH-0159".
   *
   * @param name the name of the entry to fetch
   * @return data-stream of the operation reference source in the eCH model
   *
   * @throws IllegalArgumentException if there is no entry with the given name
   * @see #getOperationReferenceNames()
   */
  public InputStream getOperationReferenceSource(String name) {
    if ( ! operationReferenceFilenames.contains(name)) {
      throw new IllegalArgumentException("\"" + name + "\" is not one of the operation references in this archive");
    }
    return reader.getEntryAsInputStream(name);
  }

  /**
   * Access to the operationReference data. Also known as "eCH-0157" / "eCH-0159".
   * <p>
   *   Please note that all streams have been opened, thus the caller should ensure to close them all
   *   when it has finished with them.
   *   You could also access them one by one using their names.
   * </p>
   *
   * @return data-streams of the operation reference sources in the eCH model, mapped by their names
   *
   * @see #getOperationReferenceNames()
   * @see #getOperationReferenceSource(String)
   */
  public Map<String, InputStream> getOperationReferenceSources() {
    return operationReferenceFilenames.stream().collect(Collectors.toMap(
        Function.identity(),
        reader::getEntryAsInputStream
    ));
  }
}

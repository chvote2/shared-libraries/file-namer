/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * A structural component used in {@link ArchiveContentValidator} to define a group of entries,
 * the name of the group and how to verify that an entry belongs to it.
 */
final class ContentGroup {
  private final String            name;
  private final Predicate<String> condition;

  /**
   * Creates a new group.
   *
   * @param name an name that makes the group identifiable
   * @param condition the condition (applied to the entry name) that defines a group member
   */
  ContentGroup(String name, Predicate<String> condition) {
    this.name = Objects.requireNonNull(name, "Group name cannot be null");
    this.condition = Objects.requireNonNull(condition, "Condition cannot be null");
  }

  public String getName() {
    return name;
  }

  public Predicate<String> getCondition() {
    return condition;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentGroup group = (ContentGroup) o;
    return Objects.equals(name, group.name) &&
           Objects.equals(condition, group.condition);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, condition);
  }
}

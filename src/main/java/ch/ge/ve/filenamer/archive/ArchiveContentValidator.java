/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import java.io.UncheckedIOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A component to validate the content of an archive.
 * <p>
 *   This validator lets define several mandatory entry names (exact match required), and several
 *   {@link ContentGroup groups} that could be used to verify the presence of some freely named files.
 * </p>
 */
class ArchiveContentValidator {

  /** The group to which all entries matching nothing else will be associated */
  static final ContentGroup DEFAULT_GROUP = new ContentGroup("_default_", p -> true);

  private final Set<String>       expectedEntries = new HashSet<>();
  private final Set<ContentGroup> expectedGroups  = new HashSet<>();

  /**
   * Defines all entry names that must be present (exact match required) for an archive to be considered valid.
   *
   * @param expectedEntries the names of all expected entries
   */
  public void setExpectedEntries(Collection<String> expectedEntries) {
    if ( ! this.expectedEntries.isEmpty()) {
      this.expectedEntries.clear();
    }
    this.expectedEntries.addAll(expectedEntries);
  }

  /**
   * Defines a group of entries that should be contained in the archive.
   * <p>
   *   Group names must be unique within the validator. There must be at least one entry matching the group
   *   condition for the archive to be considered valid.
   * </p>
   *
   * @param contentGroup a group to add to the validation
   */
  public void addExpectedGroup(ContentGroup contentGroup) {
    // Validate that groupName is unique :
    final String groupName = contentGroup.getName();
    for (ContentGroup group : expectedGroups) {
      if (group.getName().equals(groupName)) {
        throw new IllegalArgumentException("There is already a group with name = " + groupName);
      }
    }

    this.expectedGroups.add(contentGroup);
  }

  /**
   * Validates an archive's content by referencing its entries.
   *
   * @param entries the archive's entries
   * @return all entries belonging to the defined groups
   *
   * @throws InvalidArchiveException if a condition defined on this validator is not verified
   * @throws UncheckedIOException if an I/O error occurs
   */
  public Map<ContentGroup, List<String>> validate(Collection<String> entries) {
    return validate(entries.stream());
  }

  /**
   * Validates an archive's content by streaming the entries of the archive.
   *
   * @param entries stream of the archive's entries
   * @return all entries belonging to the defined groups
   *
   * @throws InvalidArchiveException if a condition defined on this validator is not verified
   * @throws UncheckedIOException if an I/O error occurs
   */
  public Map<ContentGroup, List<String>> validate(Stream<String> entries) {
    final Set<String> expectedAndNotFound = new HashSet<>(expectedEntries);

    // In one pass we check the presence of mandatory entries and collect the group files
    Map<ContentGroup, List<String>> mappedEntries = entries.peek(expectedAndNotFound::remove)
                                                           .filter(not(expectedEntries::contains))
                                                           .collect(Collectors.groupingBy(this::byMatchingGroup));

    if ( ! expectedAndNotFound.isEmpty()) {
      throw new InvalidArchiveException("Some mandatory files are missing : " + expectedAndNotFound);
    }

    verifyNoGroupIsEmpty(mappedEntries);
    return mappedEntries;
  }

  private static <T> Predicate<T> not(Predicate<T> predicate) {
    return predicate.negate();
  }

  private ContentGroup byMatchingGroup(String entryname) {
    for (ContentGroup group : expectedGroups) {
      if (group.getCondition().test(entryname)) {
        return group;
      }
    }
    return DEFAULT_GROUP;
  }

  private void verifyNoGroupIsEmpty(Map<ContentGroup, List<String>> mappedEntries) {
    // "mappedEntries" is created with Collectors.groupingBy, so there cannot be empty values within the Map
    final Set<String> emptyGroups = expectedGroups.stream()
                                                  .filter(not(mappedEntries::containsKey))
                                                  .map(ContentGroup::getName)
                                                  .collect(Collectors.toSet());

    if ( ! emptyGroups.isEmpty()) {
      throw new InvalidArchiveException("No files were found for some expected groups : " + emptyGroups);
    }
  }

}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import static java.util.Collections.unmodifiableList;

import ch.ge.ve.filenamer.PrinterArchiveFileName;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Component that reads a "printer archive".
 * <p>
 *   When given the path to an archive artifact, a {@code PrinterArchiveReader} can access its metadata,
 *   and easily provide streams to each data it contains.
 * </p>
 * <p>
 *   This reader keeps hidden the actual structure of the archive. Used in conjunction with {@link PrinterArchiveMaker},
 *   this structure is left abstract and could be changed with minimal impact on the rest of the codebase.
 * </p>
 */
public class PrinterArchiveReader {

  private final PrinterArchiveFileName fileName;

  private final List<String> operationReferenceFilenames;
  private final List<String> privateCredentialsFilenames;

  private final ArchiveReader reader;

  private PrinterArchiveReader(ArchiveReader reader) {
    this.fileName = PrinterArchiveFileName.parse(reader.getArchiveName());

    final ArchiveContentValidator validator = new ArchiveContentValidator();
    validator.setExpectedEntries(allFixedFileNames());
    validator.addExpectedGroup(Internals.OPERATION_REFERENCE_GROUP);
    validator.addExpectedGroup(Internals.PRIVATE_CREDENTIALS_GROUP);

    final Map<ContentGroup, List<String>> entries = validator.validate(reader.getEntryNames());

    this.operationReferenceFilenames = unmodifiableList(entries.get(Internals.OPERATION_REFERENCE_GROUP));
    this.privateCredentialsFilenames = unmodifiableList(entries.get(Internals.PRIVATE_CREDENTIALS_GROUP));

    this.reader = reader;
  }

  /**
   * Instantiate a reader, bound to a specific archive defined by its path.
   * <p>
   *   This instance holds a reference to the path to the archive, but every access to any of its resources
   *   will cause a new stream to be opened from it. This is adequate if you want to access parts of the archive
   *   on a (relative) long period. If you need to access all (or part) of the resources once and then forget about
   *   the archive, prefer the {@link #withArchive(Path, Consumer)} construct.
   * </p>
   * <p>
   *   Note that the archive will be verified first, so that any invalid archive will cause this constructor
   *   to fail, and the archive will not be readable.
   * </p>
   *
   * @param archivePath path to the archive to read
   *
   * @throws InvalidArchiveException if the path does not denote a regular printer archive
   * @throws IllegalArgumentException if the archive name does not match the expected format
   * @throws UncheckedIOException if an I/O error occurs
   */
  public static PrinterArchiveReader readFrom(Path archivePath) {
    return new PrinterArchiveReader(new ReentrantArchiveReader(archivePath));
  }

  /**
   * Gets a {@link PrinterArchiveReader} to perform some operation, then closes all FS resources.
   * <p>
   *   The reader holds a single reference to the underlying file in the filesystem. It will be passed
   *   to the consumer that can freely access all of its resources. When done (or if an exception occurs),
   *   all filesystem locks are released and any subsequent call to read one of its resources will fail.
   * </p>
   * <p>
   *   Note that the archive will be verified first, so that any invalid archive will cause this method
   *   to fail before the consumer is called.
   * </p>
   *
   * @param archivePath path to the archive to read
   * @param consumer the action to perform using the reader
   *
   * @throws InvalidArchiveException if the path does not denote a regular printer archive
   * @throws IllegalArgumentException if the archive name does not match the expected format
   * @throws UncheckedIOException if an I/O error occurs
   */
  public static void withArchive(Path archivePath, Consumer<PrinterArchiveReader> consumer) {
    try (SingleEntranceArchiveReader reader = new SingleEntranceArchiveReader(archivePath)) {
      consumer.accept(new PrinterArchiveReader(reader));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private static List<String> allFixedFileNames() {
    return Arrays.asList(Internals.ECH_0045, Internals.PUBLIC_PARAMETERS, Internals.ELECTION_SET,
                         Internals.OPERATION_CONFIGURATION, Internals.ELECTION_CONFIGURATIONS);
  }

  /**
   * @return the name of the printer this archive is intended to
   */
  public String getPrinterName() {
    return fileName.getPrinterName();
  }

  /**
   * @return the name of the operation described in this archive
   */
  public String getOperationName() {
    return fileName.getOperationName();
  }

  /**
   * @return date when the archive has been created
   */
  public Optional<LocalDateTime> getCreationDate() {
    return fileName.getCreationDate();
  }

  /**
   * Access to the "voters reference" data. Also known as "eCH-0045".
   *
   * @return data-stream of the voter reference source in the eCH model
   */
  public InputStream getVotersReferenceSource() {
    return reader.getEntryAsInputStream(Internals.ECH_0045);
  }

  /**
   * Access to the publicParameters data.
   *
   * @return data-stream of the "public-parameters" source in the protocol model
   */
  public InputStream getPublicParameters() {
    return reader.getEntryAsInputStream(Internals.PUBLIC_PARAMETERS);
  }

  /**
   * Access to the electionSet data..
   *
   * @return data-stream of the "election-set" source in the protocol model
   */
  public InputStream getElectionSetSource() {
    return reader.getEntryAsInputStream(Internals.ELECTION_SET);
  }

  /**
   * Access to the operationConfiguration data.
   *
   * @return data-stream of the "operation configuration" source
   */
  public InputStream getOperationConfigurationSource() {
    return reader.getEntryAsInputStream(Internals.OPERATION_CONFIGURATION);
  }

  /**
   * Access to the electionConfigurations data.
   *
   * @return data-stream of the "election configurations" source
   */
  public InputStream getElectionConfigurationsSource() {
    return reader.getEntryAsInputStream(Internals.ELECTION_CONFIGURATIONS);
  }

  /**
   * @return a list of the names of all operation references entries
   */
  public List<String> getOperationReferenceNames() {
    return operationReferenceFilenames;
  }

  /**
   * Access to one operationReference entry. Also known as "eCH-0157" / "eCH-0159".
   *
   * @param name the name of the entry to fetch
   * @return data-stream of the operation reference source in the eCH model
   *
   * @throws IllegalArgumentException if there is no entry with the given name
   * @see #getOperationReferenceNames()
   */
  public InputStream getOperationReferenceSource(String name) {
    if ( ! operationReferenceFilenames.contains(name)) {
      throw new IllegalArgumentException("\"" + name + "\" is not one of the operation references in this archive");
    }
    return reader.getEntryAsInputStream(name);
  }

  /**
   * Access to the operationReference data. Also known as "eCH-0157" / "eCH-0159".
   * <p>
   *   Please note that all streams have been opened, thus the caller should ensure to close them all
   *   when it has finished with them.
   *   You could also access them one by one using their names.
   * </p>
   *
   * @return data-streams of the operation reference sources in the eCH model, mapped by their names
   *
   * @see #getOperationReferenceNames()
   * @see #getOperationReferenceSource(String)
   */
  public Map<String, InputStream> getOperationReferenceSources() {
    return operationReferenceFilenames.stream().collect(Collectors.toMap(
        Function.identity(),
        reader::getEntryAsInputStream
    ));
  }

  /**
   * @return a list of the names of all private credentials entries
   */
  public List<String> getPrivateCredentialsNames() {
    return privateCredentialsFilenames;
  }

  /**
   * Access to one privateCredentials entry. Also known as "epf" files.
   *
   * @param name the name of the entry to fetch
   * @return data-stream of the private credentials source
   *
   * @throws IllegalArgumentException if there is no entry with the given name
   * @see #getPrivateCredentialsNames()
   */
  public InputStream getPrivateCredentialsSource(String name) {
    if ( ! privateCredentialsFilenames.contains(name)) {
      throw new IllegalArgumentException("\"" + name + "\" is not one of the operation references in this archive");
    }
    return reader.getEntryAsInputStream(name);
  }

  /**
   * Access to the privateCredentials data. Also known as "epf" files, they are created by the protocol for the
   * corresponding voters.
   * <p>
   *   Please note that all streams have been opened, thus the caller should ensure to close them all
   *   when it has finished with them.
   *   You could also access them one by one using their names.
   * </p>
   *
   * @return data-streams of the voters' private credentials, mapped by their resource names
   *
   * @see #getPrivateCredentialsNames()
   * @see #getPrivateCredentialsSource(String)
   */
  public Map<String, InputStream> getPrivateCredentialsSources() {
    return privateCredentialsFilenames.stream().collect(Collectors.toMap(
        Function.identity(),
        reader::getEntryAsInputStream
    ));
  }
}

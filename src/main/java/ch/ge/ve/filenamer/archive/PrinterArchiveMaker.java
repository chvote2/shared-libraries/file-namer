/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer.archive;

import static java.util.Objects.requireNonNull;

import ch.ge.ve.filenamer.PrinterArchiveFileName;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This object will assist in creating a valid printer archive artifact.
 * <p>
 *   Using this component ensures that a successfully created archive will be correctly read by the corresponding
 *   {@link PrinterArchiveReader}, by encapsulating the contract of what the archive contains.
 * </p>
 * <p>
 *   This class offers a builder-like API, so that all the necessary sources can be defined when known and in
 *   a readable way. When all resources have been declared using the {@code with...} methods, a simple call to
 *   {@link #make(Path)} will create the archive, failing if anything is missing or inconsistent.
 * </p>
 */
public class PrinterArchiveMaker {

  private final String        printerName;
  private       String        operationName;
  private       LocalDateTime creationDate;

  private Path       votersReferenceSource;
  private Path       publicParametersSource;
  private Path       electionSetSource;
  private Path       operationConfigurationSource;
  private Path       electionConfigurationsSource;
  private List<Path> operationReferenceSources = new ArrayList<>();
  private List<Path> privateCredentialsSources = new ArrayList<>();

  /**
   * Instantiate a new maker object.
   *
   * @param printerName the printer this archive is intended for
   */
  public PrinterArchiveMaker(String printerName) {
    this.printerName = requireNonNull(printerName, "Null printerName not allowed");
  }

  /**
   * Define the operationName
   *
   * @param operationName the name of the operation
   */
  public PrinterArchiveMaker withOperationName(String operationName) {
    this.operationName = requireNonNull(operationName, "Null operationName not allowed");
    return this;
  }

  /**
   * Define the creationDate. This parameter is optional, {@code LocalDateTime.now()} will be used if not set.
   *
   * @param creationDate the creation date to indicate for this archive
   */
  public PrinterArchiveMaker withCreationDate(LocalDateTime creationDate) {
    this.creationDate = requireNonNull(creationDate, "Null creationDate not allowed");
    return this;
  }

  /**
   * Define the votersReference source. Also known as "eCH-0045".
   *
   * @param votersReferenceSource the reference source for the voters (eCH)
   */
  public PrinterArchiveMaker withVotersReferenceSource(Path votersReferenceSource) {
    this.votersReferenceSource = requireNonNull(votersReferenceSource);
    return this;
  }

  /**
   * Define the publicParameters source.
   *
   * @param publicParametersSource the source for the "public parameters" in the protocol model
   */
  public PrinterArchiveMaker withPublicParametersSource(Path publicParametersSource) {
    this.publicParametersSource = requireNonNull(publicParametersSource);
    return this;
  }

  /**
   * Define the electionSet source.
   *
   * @param electionSetSource the source for the "election-set" in the protocol model
   */
  public PrinterArchiveMaker withElectionSetSource(Path electionSetSource) {
    this.electionSetSource = requireNonNull(electionSetSource);
    return this;
  }

  /**
   * Define the operationConfiguration source.
   *
   * @param operationConfigurationSource the source for the "operation configuration"
   */
  public PrinterArchiveMaker withOperationConfigurationSource(Path operationConfigurationSource) {
    this.operationConfigurationSource = requireNonNull(operationConfigurationSource);
    return this;
  }

  /**
   * Define the electionConfigurations source.
   *
   * @param electionConfigurationsSource the source for the "election configurations"
   */
  public PrinterArchiveMaker withElectionConfigurationsSource(Path electionConfigurationsSource) {
    this.electionConfigurationsSource = requireNonNull(electionConfigurationsSource);
    return this;
  }

  /**
   * Define all operationReference sources. Also known as "eCH-0157" / "eCH-0159".
   * <p>
   *   All files must have a ".xml" extension, as they are stored with their original names and the extension will
   *   be used to distinguished them.
   * </p>
   * <p>
   *   Each call to this method will completely replace all operation references by the supplied ones. Note that if
   *   any of the path does not end with the right extension, the method will fail and be reverted.
   * </p>
   *
   * @param operationReferenceSources the reference sources for the votes (eCH)
   */
  public PrinterArchiveMaker withOperationReferenceSources(Collection<? extends Path> operationReferenceSources) {
    this.operationReferenceSources = operationReferenceSources.stream()
                                                              .peek(Internals::verifyOperationReference)
                                                              .collect(Collectors.toList());
    return this;
  }

  /**
   * Add an operationReference source to the currently registered ones. Also known as "eCH-0157" / "eCH-0159".
   *
   * @param operationReferenceSource the reference source for the votes (eCH - xml) to add
   * @see #withOperationReferenceSources(Collection)
   */
  public PrinterArchiveMaker addOperationReferenceSource(Path operationReferenceSource) {
    this.operationReferenceSources.add(Internals.verifyOperationReference(operationReferenceSource));
    return this;
  }

  /**
   * Define all privateCredentials sources. Also known as "epf" files and created by the protocol for the
   * corresponding voters.
   * <p>
   *   All files must have a ".epf" extension, as they are stored with their original names and the extension will
   *   be used to distinguished them.
   * </p>
   * <p>
   *   Each call to this method will completely replace all privateCredentials by the supplied ones. Note that if
   *   any of the path does not end with the right extension, the method will fail and be reverted.
   * </p>
   *
   * @param privateCredentialsSources the reference sources for the voters' private credentials
   */
  public PrinterArchiveMaker withPrivateCredentialsSources(Collection<? extends Path> privateCredentialsSources) {
    this.privateCredentialsSources = privateCredentialsSources.stream()
                                                              .peek(Internals::verifyPrivateCredentials)
                                                              .collect(Collectors.toList());
    return this;
  }

  /**
   * Add a privateCredentials source to the currently registered ones. Also known as "epf" files and created by
   * the protocol for the corresponding voters.
   *
   * @param privateCredentialsSource the reference source for the voters' private credentials (epf) to add
   * @see #withPrivateCredentialsSources(Collection)
   */
  public PrinterArchiveMaker addPrivateCredentialsSource(Path privateCredentialsSource) {
    this.privateCredentialsSources.add(Internals.verifyPrivateCredentials(privateCredentialsSource));
    return this;
  }

  private String getArchiveName() {
    return new PrinterArchiveFileName(printerName, operationName, creationDate).toString();
  }

  /**
   * Do create the artifact that will serve as the "printer archive".
   * <p>
   *   This method can be called when all properties have been set. Only then will the actual archive file
   *   be created, and all previously defined paths will be streamed into it.
   * </p>
   * <p>
   *   Please note that the file name should not be changed if you want to be able to consistently interpret
   *   the archive's metadata using {@link PrinterArchiveReader}.
   * </p>
   *
   * @param outputDirectory the directory where the archive should be created
   * @return the path to the archive artifact
   *
   * @throws IllegalArgumentException if the given outputDirectory is not a directory
   * @throws NullPointerException if any required value is missing
   * @throws UncheckedIOException if an I/O error occurs
   */
  public Path make(Path outputDirectory) {
    final Archive archive = new Archive(getArchiveName());
    archive.append(votersReferenceSource, Internals.ECH_0045);
    archive.append(publicParametersSource, Internals.PUBLIC_PARAMETERS);
    archive.append(electionSetSource, Internals.ELECTION_SET);
    archive.append(operationConfigurationSource, Internals.OPERATION_CONFIGURATION);
    archive.append(electionConfigurationsSource, Internals.ELECTION_CONFIGURATIONS);
    archive.append(operationReferenceSources, "Operation references (eCH-157 / eCH-159)");
    archive.append(privateCredentialsSources, "Private credentials (epf)");

    return archive.create(outputDirectory);
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - file-namer                                                                                     -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.filenamer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents an epf's file name. This class shall be used to format the file name for an epf from its associated
 * metadata and, conversely, to parse/read these same metadata from its file name.
 */
public final class EpfFileName {

  private static final String  EXTENSION = "epf";
  private static final String  FORMAT    = "%s_CC%d_%s.%s";
  private static final Pattern PATTERN   = Pattern.compile("([\\w\\-]+)_CC(\\d+)_(\\d+-\\d+)\\." + EXTENSION);

  /**
   * Parses the epf file name located at the given path.
   *
   * @param path the epf file's path.
   *
   * @return the parsed file name.
   *
   * @throws NullPointerException     if {@code path} is {@code null}.
   * @throws IllegalArgumentException if the file located at the given path is not an epf.
   */
  public static EpfFileName parse(Path path) {
    return parse(path.getFileName().toString());
  }

  /**
   * Parses the given "raw" epf file name.
   *
   * @param fileName the "raw" epf file name to parse.
   *
   * @return the parsed file name.
   *
   * @throws NullPointerException     if {@code fileName} is {@code null}.
   * @throws IllegalArgumentException if the given file name is not an epf file name.
   */
  public static EpfFileName parse(String fileName) {
    Matcher matcher = PATTERN.matcher(fileName);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Not an epf file name: " + fileName);
    }
    String printingAuthorityName = CaseFormat.fromKebabCase(matcher.group(1));
    int controlComponentIndex = Integer.parseInt(matcher.group(2));
    String qualifier = matcher.group(3);
    return new EpfFileName(printingAuthorityName, controlComponentIndex, qualifier);
  }

  private final String printingAuthorityName;
  private final int    controlComponentIndex;
  private final String qualifier;
  private final String fileName;

  /**
   * Creates a new {@code EpfFileName} from the the epf's metadata.
   *
   * @param printingAuthorityName the name of the printing authority the epf has been generated for, must contain only
   *                              ASCII letters, numbers and hyphens.
   * @param controlComponentIndex the index of the control component that generated the epf.
   * @param qualifier             the epf's qualifier (the range of voter indexes covered by the epf, must be in the
   *                              startIndex-endIndex format).
   *
   * @throws NullPointerException     if one of the given arguments is {@code null}.
   * @throws IllegalArgumentException if either the printing authority's name or the qualifier does not conform to
   *                                  its expected format.
   */
  public EpfFileName(String printingAuthorityName, int controlComponentIndex, String qualifier) {
    this.printingAuthorityName = Objects.requireNonNull(printingAuthorityName);
    this.controlComponentIndex = controlComponentIndex;
    this.qualifier = Objects.requireNonNull(qualifier);
    this.fileName = String.format(FORMAT, CaseFormat.toKebabCase(printingAuthorityName),
                                  controlComponentIndex, qualifier, EXTENSION);
    if (!PATTERN.matcher(this.fileName).matches()) {
      throw new IllegalArgumentException("Invalid format for printing authority name or qualifier.");
    }
  }

  /**
   * Returns the name of the printing authority the epf has been generated for.
   *
   * @return the printing authority's name.
   */
  public String getPrintingAuthorityName() {
    return printingAuthorityName;
  }

  /**
   * Returns the index of the control component that generated the epf.
   *
   * @return the control component's index.
   */
  public int getControlComponentIndex() {
    return controlComponentIndex;
  }

  /**
   * Returns the qualifier of the epf (the range of voter indexes covered by the epf).
   *
   * @return the qualifier of the epf.
   */
  public String getQualifier() {
    return qualifier;
  }

  /**
   * Transforms and returns this file name into a relative path.
   *
   * @return this file name as a relative path.
   */
  public Path toPath() {
    return Paths.get(fileName);
  }

  @Override
  public String toString() {
    return fileName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EpfFileName that = (EpfFileName) o;
    return Objects.equals(fileName, that.fileName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fileName);
  }
}
